//Meagan Fagan
//MTN App Academy Module 2 Assessment - Question 1

//Write a basic program that stores and then prints the following data: Your name, favorite app, and city;
void main() {
  String name = "Meagan Fagan";
  String favourite_app = "Twitter";
  String city = "Cape Town";

  print("My name is $name");
  print("My favourite app to use is $favourite_app");
  print("I reside in $city");
}
