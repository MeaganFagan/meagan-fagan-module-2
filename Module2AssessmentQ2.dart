//Meagan Fagan
//MTN App Academy Module 2 Assessment - Question 2

//Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;
//a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.;
//c) the Print total number of apps from the array.
void main() {
  var arr = [
    'FNB Banking',
    'Snapscan',
    'LIVE Inspect',
    'WumDrop',
    'Domestly',
    'Shyft',
    'Khula ecosystem',
    'Naked Insurance',
    'EasyEquities',
    'Ambani Africa'
  ];
  String winner_2017 = arr[5].toString();
  String winner_2018 = arr[6].toString();
  arr.sort();
  print("List of winning apps from 2012 to 2021 sorted by name:");
  print("$arr");

  //b
  print("The winning app of 2017 is " + winner_2017);
  print("The winning app of 2018 is " + winner_2018);

  //c
  print("Number of apps in the array is: " + arr.length.toString());
}
