//Meagan Fagan
//MTN App Academy Module 2 Assessment - Question 3

//Create a class and a) then use an object to print the name of the app, sector/category, developer, and the year
// it won MTN Business App of the Year Awards.b) Create a function inside the class, transform the app name
//to all capital letters and then print the output.

void main() {
  dClass dc = new dClass();
  dc.displayMessage();
  dc.allCaps();
}

class dClass {
  String appname = "Ambani Africa";
  String category = "Overall Winner";
  String developer = "Mukundi Lambani";
  int year_won = 2021;

  void displayMessage() {
    print("App name: " +appname+ " Category: " + category+ " Developer: " + developer+ "  Year won: " + year_won.toString());
  }

  void allCaps() {
    print(appname.toUpperCase());
  }
}
